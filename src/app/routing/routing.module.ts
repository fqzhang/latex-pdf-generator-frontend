import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UploadVersionComponent } from '../mrtt/upload-version/upload-version.component';
import { FormVersionComponent } from '../mrtt/form-version/form-version.component';
import { MrttComponent } from '../mrtt/mrtt.component';
const routes: Routes = [
  { path: '', redirectTo: '/mrtt', pathMatch: 'full' },
  { path: 'mrtt', component: MrttComponent },
  { path: 'mrtt/form-version', component: FormVersionComponent },
  { path: 'mrtt/upload-version', component: UploadVersionComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class RoutingModule { }
