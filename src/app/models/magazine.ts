import { Article } from './article';

export class Magazine {
    left_head_title: string;
    middle_head_title: string;
    right_head_title: string;
    articles: Article[];
}
