export class Article {
    title: string;
    link?: string;
    footnote?: string;
    content: string[];
    sequence?: number;
}
