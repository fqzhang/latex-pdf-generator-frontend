import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoadingModule } from 'ngx-loading';
import { RoutingModule } from './routing/routing.module';
import { MrttComponent } from './mrtt/mrtt.component';
import { FormVersionComponent } from './mrtt/form-version/form-version.component';
import { UploadVersionComponent } from './mrtt/upload-version/upload-version.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { NgxUploaderModule } from 'ngx-uploader';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

library.add(fas, far);
@NgModule({
  declarations: [
    AppComponent,
    FormVersionComponent,
    UploadVersionComponent,
    MrttComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    LoadingModule,
    RoutingModule,
    FontAwesomeModule,
    NgxUploaderModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
