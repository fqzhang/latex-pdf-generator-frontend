import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver/FileSaver';
import { Magazine } from '../../models/magazine';
import { Article } from '../../models/article';

@Component({
  selector: 'app-form-version',
  templateUrl: './form-version.component.html',
  styleUrls: ['./form-version.component.css']
})
export class FormVersionComponent implements OnInit {
  title = 'app';
  article: Article;
  magazine: Magazine;
  public loading = false;
  constructor(
    private http: HttpClient
  ) {
    this.magazine = new Magazine();
    this.magazine.left_head_title = '翻墙者杂志';
    this.magazine.middle_head_title = '明日头条';
    this.magazine.right_head_title = '第x期';
    this.magazine.articles = [];
    this.article = new Article();
    this.article.sequence = 1;
    this.article.content = [];
    this.article.content.push('');
  }

  ngOnInit() {
  }

  insertNewParagraph() {
    this.article.content.push('');
  }
  editArticle(article: Article) {
    this.article = article;
  }
  removeParagraph(index: number) {
    this.article.content.splice(index, 1);
  }
  removeArticle(index: number) {
    this.magazine.articles.splice(index, 1);
  }
  addarticle(article: Article) {
    this.magazine.articles.push(article);
    this.resetArticle();
  }
  updateParagraphContent(text: string, index: number) {
    this.article.content[index] = text;
  }
  resetArticle() {
    this.article = new Article();
    this.article.sequence = this.magazine.articles.length + 1;
    if (this.article.sequence < 1) {
      this.article.sequence = 1;
    }
    this.article.content = [];
    this.article.content.push('');
  }

  generatePdfFile() {
    if (this.magazine.articles && this.magazine.articles.length > 0) {
      this.loading = true;
      this.http.post('https://api.chenhang.app/generate-pdf', this.magazine, {
        responseType: 'blob'
      }).subscribe((data) => {
        const blob = new Blob([data], { type: 'application/pdf' });
        saveAs(data, this.magazine.middle_head_title + this.magazine.right_head_title + '.pdf');
        this.loading = false;
      }, err => {
        this.loading = false;
        console.log(JSON.stringify(err));
      });
    } else {
      alert('请添加至少一则新闻');
    }
  }
}
