import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MrttComponent } from './mrtt.component';

describe('MrttComponent', () => {
  let component: MrttComponent;
  let fixture: ComponentFixture<MrttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MrttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MrttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
