import { Component, OnInit, EventEmitter } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';

@Component({
  selector: 'app-upload-version',
  templateUrl: './upload-version.component.html',
  styleUrls: ['./upload-version.component.css']
})
export class UploadVersionComponent implements OnInit {
  options: UploaderOptions;
  formData: FormData;
  file: UploadFile;
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  fileName = '请选择文件...';
  uploadCompleted = false;
  pdfFileLink = '';
  texFileLink = '';
  message = '';
  constructor() {
    this.options = {
      concurrency: 1,
      allowedContentTypes: ['application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain']
    };
    this.file = null; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  ngOnInit() {
  }
  onUploadOutput(output: UploadOutput): void {
    console.log(JSON.stringify(output));
    if (this.file) {
      const progressBar = document.getElementById('upload-progress-bar');
      progressBar.setAttribute('aria-valuenow', this.file.progress.data.percentage.toString());
      progressBar.style.width = `${this.file.progress.data.percentage.toString()}%`;
      if (this.file.progress.status === 2) {  // 上传完成
        this.uploadCompleted = true;
        this.pdfFileLink = `https://api.chenhang.app/${this.file.response[0]}`;
        this.texFileLink = `https://api.chenhang.app/${this.file.response[1]}`;
        this.file = null;
        this.fileName = '请选择文件...';
        this.message = '处理成功, 可以开始点击下方的链接下载文件了. ^_^';
      } else if (this.file.progress.status === 1) { // 正在上传
        this.message = `正在上传文件, 已完成 ${this.file.progress.data.percentage.toString()}%`;
        if (this.file.progress.data.percentage === 100) {
          this.message = `上传完成！ 正在等待服务器处理，请稍等...`;
        }
      } else if (this.file.progress.status === 0) { // 选了文件还没上传
        this.message = `一切准备就绪，可以点击'开始上传'按钮开始上传了！`;
      }
    }
    if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') { // when all files added in queue
      this.file = output.file;
      this.fileName = this.file.name;
      this.uploadCompleted = false;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') { // 文件格式不支持或已经选择了一个文件
      if (output.file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
        output.file.type === 'text/plain') { // 上传的是支持的文件类型，替换之前选择的文件
        this.file = output.file;
        this.fileName = this.file.name;
        this.uploadCompleted = false;
      } else { // 上传的文件不是 word 或 txt
        alert('上传的文件不是word 或 txt, 请上传正确格式的文件.');
      }
    }
  }

  startUpload(): void {
    this.message = '开始上传文件, 请稍后...';
    // console.log('uploading status : ' + this.file.progress.status.toString());
    const event: UploadInput = {
      type: 'uploadFile',
      // url: 'https://api.chenhang.app/fqz/upload',
      url: 'https://api.chenhang.app/fqz/upload',
      method: 'POST',
      file: this.file
    };
    this.uploadInput.emit(event);
  }
}
