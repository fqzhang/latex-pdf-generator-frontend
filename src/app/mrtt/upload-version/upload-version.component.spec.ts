import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadVersionComponent } from './upload-version.component';

describe('UploadVersionComponent', () => {
  let component: UploadVersionComponent;
  let fixture: ComponentFixture<UploadVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
